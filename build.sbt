val akkaGroup = "com.typesafe.akka"
val akkaVer = "2.4.1"
val httpVer = "2.0.1"

val http = Seq(
  akkaGroup %% "akka-stream-experimental" % httpVer,
  akkaGroup %% "akka-http-core-experimental" % httpVer,
  akkaGroup %% "akka-http-experimental" % httpVer,
  akkaGroup %% "akka-http-testkit-experimental" % httpVer
)

val akka = Seq(
  akkaGroup %% "akka-actor" % akkaVer,
  akkaGroup %% "akka-persistence" % akkaVer,
  akkaGroup %% "akka-testkit" % akkaVer,
  akkaGroup %% "akka-slf4j" % akkaVer
)

val testing = Seq(
  "org.scalatest" %% "scalatest" % "2.2.4" % "test"
)

val reactiveMongoGroup = "org.reactivemongo"


val mongo = Seq(
    reactiveMongoGroup %% "reactivemongo" % "0.11.10",
    reactiveMongoGroup %% "reactivemongo-extensions-bson" % "0.11.7.play24"
  ,"org.mongodb" %% "casbah" % "3.1.0"
)

val akkaPersistenceMongo = Seq(
//  "com.github.scullxbones" %% "akka-persistence-mongo-rxmongo" % "1.2.1"
  "com.github.scullxbones" %% "akka-persistence-mongo-casbah" % "1.2.1"
)

val json = Seq(
  "org.json4s" %% "json4s-jackson" % "3.2.11",
  "de.heikoseeberger" %% "akka-http-json4s" % "1.0.0"
)

val logging = Seq(
  "ch.qos.logback" % "logback-classic" % "1.1.3"
)

val config = Seq(
  "com.typesafe" % "config" % "1.3.0"
)

val akkaPersistencePlugins = Seq(
  "com.github.dnvriend" %% "akka-persistence-inmemory" % "1.1.4"
)


lazy val fatJar = Seq(
  assemblyJarName in assembly := "something.jar",
  test in assembly := {},
  assemblyMergeStrategy in assembly := {

    case PathList(ps@_*) if ps.last endsWith "log4j-provider.properties" => MergeStrategy.last
    case x =>
      val oldStrategy = (assemblyMergeStrategy in assembly).value
      oldStrategy(x)
  },
  mainClass in assembly := Some("io.prods.engine.LifecycleManager")
)


lazy val commonSettings = Seq(
  organization := "io.prods",
  version := "0.0.2",
  scalaVersion := "2.11.7",

  resolvers ++= Seq(
    "Typesafe repository releases" at "http://repo.typesafe.com/typesafe/releases/",
    "dnvriend at bintray" at "http://dl.bintray.com/dnvriend/maven/"
  )

)


/** Utilities project */
lazy val util = (project in file("util")).
  settings(commonSettings: _*).
  settings(
    name := "prods-engine-utilities",
    libraryDependencies ++= config ++ akka ++ testing
  )


/** Core project */
lazy val `prods-engine-core` = project in file("core")


/** Catalogue project */
lazy val catalogue = (project in file("catalogue")).
  settings(commonSettings: _*).
  settings(
    name := "prods-engine-catalogue",
    libraryDependencies ++= akka ++ testing ++ http ++ mongo ++ json ++ logging
  ).
  dependsOn(util)


/** Editorial project */
lazy val editorial = (project in file("editorial")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= akka ++ testing ++ http ++ json ++ logging
  ).dependsOn(util)




/** Root engine project */
lazy val engine = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    name := "prods-engine",
    //    libraryDependencies ++= akka ++ testing ++ http ++ mongo ++ json ++ logging ++ config,
    mainClass in(Compile, run) := Some("io.prods.engine.LifecycleManager")
  ).
  settings(fatJar: _*).
  dependsOn(catalogue).
  aggregate(`prods-engine-core`, catalogue, editorial, util)
