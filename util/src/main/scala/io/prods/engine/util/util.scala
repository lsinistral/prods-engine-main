package io.prods.engine

import java.util.UUID

package object util {
  def generateID: UUID = {
    UUID.randomUUID()
  }
}
