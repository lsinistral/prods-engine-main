package io.prods.engine.util

import akka.actor.{ActorLogging, Actor}

abstract class LoggingActor extends Actor with ActorLogging
