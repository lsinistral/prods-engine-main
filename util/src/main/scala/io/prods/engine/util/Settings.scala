package io.prods.engine.util

import com.typesafe.config.ConfigFactory

/**
 * Object represents application settings and configuration
 * by default uses "application.conf" file
 */
object Settings {

  private[this] val config = ConfigFactory.load()


  final val mongoDatabase: String = config.getString("prods.mongo.database")
  final val mongoHost: String = config.getString("prods.mongo.host")

  final val catalogueListenAddress = config.getString("prods.catalogue.listenAddress")
  final val catalogueListenPort = config.getInt("prods.catalogue.listenPort")
}
